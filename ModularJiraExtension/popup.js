$(function () {
	$('#store').hide();
	$('#legacyBug').hide();
	$('#bug').hide();
	$('#techSolution').hide();
	$('#subTasks').hide();
});

$(document).on("change","input[type=radio]",function(){
	
    var radioChecked = $('input[type=radio]:checked').val();
	
    switch (radioChecked)
	{
		case '1':
			$('#store').show();
			$('#legacyBug').hide();
			$('#bug').hide();
			$('#techSolution').hide();
			break;
		case '2':
			$('#store').hide();
			$('#legacyBug').show();
			$('#bug').hide();
			$('#techSolution').hide();
			break;
		case '3': 
			$('#store').hide();
			$('#legacyBug').hide();
			$('#bug').show();
			$('#techSolution').hide();
			break;
		case '4': 
			$('#store').hide();
			$('#legacyBug').hide();
			$('#bug').hide();
			$('#techSolution').show();
			break;

	   default: 
		   $('#error-message').text('Unrecognizable radio button value!');
	}
	
});

$(document).on("change","input[name=hasSubTask]",function(){
	if (document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) {
        $('#subTasks').show();
    } 
	else {
        $('#subTasks').hide();
    }
	
});

document.addEventListener('DOMContentLoaded', function() {
  var createButton = document.getElementById('createButton');
  createButton.addEventListener('click', function() {
	
	var radioChecked = $('input[type=radio]:checked').val();
	jiraTask = createJiraJsonStringify(radioChecked);
	jiraTask = JSON.stringify(jiraTask);
	
	if(jiraTask != null && jiraTask != "undefined" && jiraTask != ""){
		$.ajax(
		{	
			type: "POST",
			url: "https://wsgateway.cit.com.br/rest/api/2/issue/",
			dataType: "JSON",
			contentType: "application/json; charset=utf-8",
			data: jiraTask,
			
			beforeSend: function (request){ 
				request.setRequestHeader('Authorization', makeBaseAuth(user.name, user.password)); 
				request.setRequestHeader('app_token', 'gZlV9pKvSCZx'); 
			},

			success: function (sucessObject){
				$('#success-task').text('Task [' + sucessObject.key + '] has been created with sucess! Link: https://jiracloud.cit.com.br/browse/' + sucessObject.key);
				
				jiraSubTask = createSubTaskJiraJsonStringify(sucessObject.key);
				
				if(jiraSubTask.issueUpdates.length > 0){
					$.ajax(
					{	
						type: "POST",
						url: "https://wsgateway.cit.com.br/rest/api/2/issue/bulk",
						dataType: "JSON",
						contentType: "application/json; charset=utf-8",
						data: JSON.stringify(jiraSubTask),
						
						beforeSend: function (request){ 
							request.setRequestHeader('Authorization', makeBaseAuth(user.name, user.password)); 
							request.setRequestHeader('app_token', 'gZlV9pKvSCZx'); 
						},

						success: function (sucessSubObject){
							var links = "Sub-Task(s) has(ve) been created with sucess! \n";
							
							sucessSubObject.issues.forEach(function(element) {
								links = links + 'https://jiracloud.cit.com.br/browse/' + element.key + '\n';
							});
							
							$('#success-subtask').text(links);
						},
						 error: function(errorSubObject){
							$('#error-message').text('Sub Task Error: [Status: ' + errorSubObject.status + '] [Message: ' + errorSubObject.statusText + ']');
						}
					});
				}
				
			},
			 error: function(errorObject){
				$('#error-message').text('Task Error: [Status: ' + errorObject.status + '] [Message: ' + errorObject.statusText + ']');
			}
		});
	}
	
	
  }, false);
}, false);

function makeBaseAuth(user, password)
{
	var tok = user + ':' + password;
	var hash = btoa(tok);
	return "Basic " + hash;
}

function createJiraJsonStringify(type)
{
	$('#error-message').text('');
	
	switch (type)
	{
		case '1':
			var storeName = document.getElementById('storeName').value;
			
			if(storeName != "" && storeName != null){
				return jira = {
					"fields": 
					{
						"project":
						{
							"key": "MDLR"
						},
						"summary": storeName,
						"issuetype": 
						{
							"name": "Story"
						}
					}
				};	
			}
			else{
				$('#error-message').text('Story name is required!');
			}
			break;
			
		case '2':
			var legacyBugName = document.getElementById('legacyBugName').value;
			var selectObject = document.getElementById('legacyBugPhase');
			var phase = selectObject.options[selectObject.selectedIndex].value;
			
			if(legacyBugName != "" && legacyBugName != null && phase != "Select one"){
				return jira = {
					"fields": 
					{
						"project":
						{
							"key": "MDLR"
						},
						"summary": legacyBugName,
						"issuetype": 
						{
							"name": "Legacy Bug"
						},
						"customfield_12510": 
						{
							"value": phase
						}
					}
				};	
			}
			else{
				$('#error-message').text('Legacy Bug name and Development Phase are required!');
			}
			break;
			
		case '3':
			var bugName = document.getElementById('bugName').value;
			var selectObject = document.getElementById('bugPhase');
			var phase = selectObject.options[selectObject.selectedIndex].value;
			
			if(bugName != "" && bugName != null && phase != "Select one"){
				return jira = {
					"fields": 
					{
						"project":
						{
							"key": "MDLR"
						},
						"summary": bugName,
						"issuetype": 
						{
							"name": "Bug"
						},
						"customfield_12510": 
						{
							"value": phase
						}
					}
				};	
			}
			else{
				$('#error-message').text('Bug name and Development Phase are required!');
			}
			
			break;
		case '4':
			var techSolutionName = document.getElementById('techSolutionName').value;
			
			if(techSolutionName != "" && techSolutionName != null){
				return jira = {
					"fields": 
					{
						"project":
						{
							"key": "MDLR"
						},
						"summary": techSolutionName,
						"issuetype": 
						{
							"name": "Tech Solution"
						}
					}
				};	
			}
			else{
				$('#error-message').text('Tech Solution name is required!');
			}
			
			break;

	   default: 
		   $('#error-message').text('Unrecognizable radio button value!');
	}
}

function createSubTaskJiraJsonStringify(jiraKey)
{
	var jsonData = {
		"issueUpdates" : []
	};
	
	var grooming = {
		"fields" : {
			"project"   : {"key" : "MDLR"},
			"parent"    : {"key" : jiraKey},
			"summary"   : "Grooming",
			"issuetype" : {"name" : "Sub-Tech Solution"},
		}
	};
	
	if ((document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) && (document.querySelectorAll("input[name='grooming']:checked").length >= 1)) {
        jsonData.issueUpdates.push(grooming);
    } 
	
	var development = {
		"fields" : {
			"project"   : {"key" : "MDLR"},
			"parent"    : {"key" : jiraKey},
			"summary"   : "Development",
			"issuetype" : {"name" : "Sub-Imp"}
		}
	};
	
	if ((document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) && (document.querySelectorAll("input[name='development']:checked").length >= 1)) {
        jsonData.issueUpdates.push(development);
    } 
	 
	var prepare = {
		"fields" : {
			"project"   : {"key" : "MDLR"},
			"parent"    : {"key" : jiraKey},
			"summary"   : "Prepare Tests",
			"issuetype" : {"name" : "Sub-Test"}
		}
	};
	
	if ((document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) && (document.querySelectorAll("input[name='prepare']:checked").length >= 1)) {
        jsonData.issueUpdates.push(prepare);
    } 
	
	var execute = {
		"fields" : {
			"project"   : {"key" : "MDLR"},
			"parent"    : {"key" : jiraKey},
			"summary"   : "Execute Tests",
			"issuetype" : {"name" : "Sub-Test"}
		}
	};
	
	if ((document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) && (document.querySelectorAll("input[name='execute']:checked").length >= 1)) {
        jsonData.issueUpdates.push(execute);
    } 
	
	var merge = {
		"fields" : {
			"project"   : {"key" : "MDLR"},
			"parent"    : {"key" : jiraKey},
			"summary"   : "Merge",
			"issuetype" : {"name" : "Sub-Imp"}
		}
	};
	
	if ((document.querySelectorAll("input[name='hasSubTask']:checked").length >= 1) && (document.querySelectorAll("input[name='merge']:checked").length >= 1)) {
        jsonData.issueUpdates.push(merge);
    } 

	return jsonData;
}

	